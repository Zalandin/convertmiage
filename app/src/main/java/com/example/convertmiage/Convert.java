package com.example.convertmiage;

import static java.lang.Integer.parseInt;
import android.os.Parcel;
import android.os.Parcelable;

public class Convert implements Parcelable {
    private final String btc;

    public Convert(String btc) {
        this.btc = btc;
    }

    protected Convert(Parcel in) {
        btc = in.readString();
    }
    public String getBTC(){
        return btc;
    }

    public String getDollar() {
        int iBtc = parseInt(btc);
        int result = iBtc * 65536;
        return String.valueOf(result);
    }

    public static final Creator<Convert> CREATOR = new Creator<Convert>() {
        @Override
        public Convert createFromParcel(Parcel in) {
            return new Convert(in);
        }

        @Override
        public Convert[] newArray(int size) {
            return new Convert[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(btc);
    }
}
