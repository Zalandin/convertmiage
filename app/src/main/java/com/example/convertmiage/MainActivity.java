
package com.example.convertmiage;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnConv = findViewById(R.id.btnConvertir);
        btnConv.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        EditText txtBTC = findViewById(R.id.inputBTC);

        if(view.getId() == R.id.btnConvertir){
            Intent inte = new Intent(this, MainActivity2.class);
            Convert nbBTC = new Convert(txtBTC.getText().toString());
            inte.putExtra("btc", nbBTC);
            this.startActivity(inte);
        }
    }
}

