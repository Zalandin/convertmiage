package com.example.convertmiage;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener{

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Button btnRetour = findViewById(R.id.btnRetour);
        btnRetour.setOnClickListener(this);

        Intent inte = this.getIntent();
        Convert info = inte.getParcelableExtra("btc");

        TextView btc2 = findViewById(R.id.txtBTC);
        btc2.setText(info.getBTC() + " BTC");

        TextView dollar = findViewById(R.id.txtDollar);
        dollar.setText(info.getDollar() + " $ ");
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnRetour){
            Intent it = new Intent(this, MainActivity.class);
            startActivity(it);
        }
    }
}